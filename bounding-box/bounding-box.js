if (Meteor.isClient) {

  var map = null;
  var features = [];
  var features_dep = new Deps.Dependency();

  var insertFeature = function(name, lat, lng) {
    console.log("Inserting new feature");
  }

  insertFeature("test", 0, 0);


  Template.map_area.dummy_data = function() {
    features_dep.depend();
    return features.length;
  };

  Template.map_area.rendered = function() {
    if (map == null) {
      map = L.map('map').setView([51.505, -0.09], 13);
      L.tileLayer('http://{s}.tile.cloudmade.com/971147a7a4a7431099a43c98f5f38ae6/997/256/{z}/{x}/{y}.png', {
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
      maxZoom: 18
      }).addTo(map);
    }
    else {
      console.log("Running update method")
      var marker = L.marker([51.5, -0.09]).addTo(map);

    }
    console.log("I rendered!")
  }

  Template.hello.events({
    'click input' : function () {
      insertFeature("test2", 10, 10);
      features_dep.changed();
    }
  });
}
